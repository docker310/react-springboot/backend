FROM maven:latest as build
WORKDIR /backend
COPY . .

RUN mvn clean install -D maven.test.skip=true

FROM openjdk:11-jre-slim-buster
ARG JAR_FILE=/backend/target/*.jar
WORKDIR /backend
COPY --from=build ${JAR_FILE} backend.jar

EXPOSE 8080

ENTRYPOINT ["java", "-jar", "backend.jar"]